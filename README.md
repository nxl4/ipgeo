# IPGeo

## General Description

**NOTE: ** *This `bash` utility is now depricated. It has been reworked into the Python utility, `igg`, which includes added functionality, and utilizes a more robust API. `igg` is available here: [https://gitlab.com/nxl4/igg](https://gitlab.com/nxl4/igg)*

A Linux CLI utility for geolocating IP addresses written in `bash`. The utility's core functionality is to retrieve either (1) a complete geolocation record for a given IP address, or (2) specific fields from that IP address's records. The utility operates by executing `GET` requests with `cURL` against the [IP Location API](https://ipapi.co/api/). The utility makes use of the free version of the API, which allows 1,000 requests daily per user (30,000 monthly). This makes the utility more suitable for *ad hoc* queries, than for batch processing. Although, the utility's output can easily be piped into dependent shell scripts.

The idea for this utility came from the built-in `whois` Linux program, and the 3rd party `shodan` utility, both of which enable investigations into open source intelligence (OSINT) data relating to IP addresses from the command line. This utility fills a gap that exists between these two other programs, providing fast access to geolocation data for any valid external IP address straight from the Linux CLI.

## Input

Data is input into the utility via CLI arguments. There are three categories of arguments accepted by the utility, all of which are optional: (1) general arguments, (2) response modes, and (3) request fields. The response mode and request field arguments *are* mutually exclusive. The utility can be used to *either* run a full query with a defined response mode, *or* a limited query with a defined request field.

### General Arguments

There are two general arguments: 

* The IP address is input as a positional argument, either immediately after the utility's initial `ipgeo` command, or following a secondary argument. This defines the specific IP address used to query the REST API.
* `--help` This argument retrieves the help messages, and prints it to the CLI.

### Response Modes

The response mode arguments define the format in which the response data is presented. There are four valid options:

* `--json` JavaScript Opject Notation Formatting
* `--yaml` Yet Another Markup Language Formatting
* `--xml` Extensible Markup Language Formatting
* `--csv` Comma Separated Value Formatting

### Request Fields

The request fields arguments define the *single* specific field returned by the query. There are [sixteen valid options](https://ipapi.co/api/#location-of-a-specific-ip):

* `--ip` public/external IP address (same as query ip)
* `--city` city name
* `--region` region name (administrative division)
* `--region_code` region code
* `--country` country code (two letter, ISO 3166-1 alpha-2)
* `--country_name` country name
* `--continent_code` continent code
* `--in_eu` whether IP address belongs to a country that is a member of European Union (EU)
* `--postal` postal code
* `--latitude` latitude
* `--longitude` longitude
* `--timezone` timezone (IANA format i.e. “Area/Location”)
* `--utc_offset` UTC offset as +HHMM or -HHMM (HH is hours, MM is minutes)
* `--country_calling_code` country calling code (dial in code, comma * `--separated)
* `--currency` currency code (ISO 4217)
* `--languages` languages spoken (comma separated 2 or 3 letter ISO 639 code with optional hyphen separated country suffix)
* `--asn` autonomous system number
* `--org` organinzation name

## Output

Depending on which optional arguments are provided, there are two general formats that the utility will present output: (1) mode specific output, and (2) field specific output.

### Mode Specific

When a mode option is selected (e.g. `--json`), then all of the available fields for the IP address's record are returned, within the defined structure. If no mode or field options are provided, the utility defaults to `--yaml` mode, which is the most clear when printed to the CLI.

### Field Specific

When a field option is selected (e.g. `--country`), then only that one data field for that IP address's record is returned, and the value is returned as a string.

## Usage

The most basic command that can be issued is to return the full YAML formatted record for your current IP address:

```shell
ipgeo
```

![ex1](img/ex1.png)

Adding a field specific option to this command will delimit the results to a specific field for your current IP address's record. For example, to retrieve the `country` value of your current IP address:

```shell
ipgeo --country
```

![ex1](img/ex2.png)

Alternatively, if you add a mode option to the root command, you can retrieve the full record for your current IP address formatted differently. For example, to retrieve your current IP address's record with `JSON` formatting:

```shell
ipgeo --json
```

![ex1](img/ex3.png)

To query a specific IP address, simply include the IP address as a positional argument immediately following the base command; this will return YAML formatted results. For example, to retrieve the record for the `2.2.2.2` address:

```shell
ipgeo 2.2.2.2
```

![ex1](img/ex4.png)

Using one of the mode options, this same output can be recormatted. For example, to reformat the above record in `JSON` structure:

```shell
ipgeo --json 2.2.2.2
```

![ex1](img/ex5.png)

Alternatively, field options can be supplied to limit the results to a single data field value for a defined IP address. For example, to retrieve the `country` value for the `2.2.2.2` address:

```shell
ipgeo --country 2.2.2.2
```

![ex1](img/ex6.png)

Additionally, the results generated by the utility can be stored as variables, for subsequent use in other `bash` scripts. For example, to variablize the `JSON` formatted results for the `2.2.2.2` IP address:

```shell
RESULTS="$(ipgeo --json 2.2.2.2)"
echo ${RESULTS}
```

![ex1](img/ex7.png)

Likewise, the same variablization can be done on single field results. For example, to capture the `country` value for the `2.2.2.2` IP address as a string:

```shell
RESULTS="$(ipgeo --country 2.2.2.2)"
echo ${RESULTS}
```

![ex1](img/ex8.png)

## Installation

The easiest way to install this utility is to first clone the repository:

```shell
git clone https://gitlab.com/nxl4/ipgeo.git
```

And then to run the `make` command from within the cloned repository's new directory:

```shell
sudo make install
```

## Issues

Please report any issues to the [Issue Tracker](https://gitlab.com/nxl4/ipgeo/issues/new) for this repository. 

## Author

nxl4: [nxl4@protonmail.com](nxl4@protonmail.com)

## License

[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html)
