PREFIX ?= /usr

all:
	@echo Run \'make install\' to install ipgeo.

install:
	@mkdir -p $(DESTDIR)$(PREFIX)/bin
	@cp -p ipgeo $(DESTDIR)$(PREFIX)/bin/ipgeo
	@chmod 755 $(DESTDIR)$(PREFIX)/bin/ipgeo

uninstall:
	@rm -rf $(DESTDIR)$(PREFIX)/bin/ipgeo
